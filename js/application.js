var map = L.map('map', {
	zoom: 4,
	minZoom: 5,
	center: [-15.8, -47.9],
	fullscreenControl: true,
}).fitBounds([[-35.9, -76.0], [7.3, -26.6]]);

function onEachFeature(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.conteudo) {
        layer.bindPopup("Local " + feature.properties.local + "<br>" + "Data " + feature.properties.local + "<br>" + "Conteudo " + feature.properties.conteudo);
     }
}

var geojsonFeature = [
{
    "type": "Feature",
    "properties": {
        "local": "São Paulo",
        "data": "01/09/2015",
        "conteudo": "Fogo na rodovia!"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [-46.6361100, -23.5475000]
    }
},
{
    "type": "Feature",
    "properties": {
        "local": "São José dos Campos",
        "data": "01/09/2015",
        "conteudo": "Futebol no CTA!"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [-48.6361100, -23.5475000]
    }
}
];

$.each(geojsonFeature, function(key, value){
	L.geoJson(geojsonFeature, {
		onEachFeature: onEachFeature
	}).addTo(map);
});

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'danielpavone.1cf06900',
    accessToken: 'pk.eyJ1IjoiZGFuaWVscGF2b25lIiwiYSI6IjhjNzE3ZDFjMjg1MjQ1NWM2ZGQ0YmI2OTJiNGJjMjgzIn0.mf4lT0MG78iAyzb1C1dFXA'
}).addTo(map);